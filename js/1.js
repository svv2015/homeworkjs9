function showList(arr, parent = document.body) {
    const list = document.createElement('ul');
    parent.appendChild(list);
  
    arr.forEach((item) => {
      const listItem = document.createElement('li');
      listItem.textContent = item;
      list.appendChild(listItem);
    });
  }
  
  const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  const arr2 = ["1", "2", "3", "sea", "user", 23];
  
  showList(arr1);
  showList(arr2);